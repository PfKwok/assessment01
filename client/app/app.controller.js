(function () {
    "use strict";
    angular.module("RegApp")
        .controller("RegCtrl", RegCtrl)
        .controller("RegisteredCtrl", RegisteredCtrl);
    
    RegCtrl.$inject = ['RegServiceAPI',"$log"]; 
    RegisteredCtrl.$inject = ['RegServiceAPI',"$log"];

    function RegisteredCtrl(RegServiceAPI, $log) {
        var self = this;
        console.log(">>> " + RegServiceAPI.getFullname());
        self.fullname = RegServiceAPI.getFullname();
    }

    function RegCtrl(RegServiceAPI, $log) {
        var regCtrlself  = this;
        
        regCtrlself.initForm = initForm;
        regCtrlself.onReset = onReset;
        regCtrlself.onSubmit = onSubmit;
        
        regCtrlself.isSubmitted = false;

        regCtrlself.user = {
            
        }

        regCtrlself.nationalities = [
            { name: "Please select", value: 0},
            { name: "Singaporean", value: 1},
            { name: "Australian", value: 2},
            { name: "Japanese", value: 3},
            { name: "American", value: 4},
            { name: "Malaysian", value: 5}     
        ];

        function initForm(){
            regCtrlself.user.selectedNationality = "0";
            regCtrlself.user.gender = "F";
            regCtrlself.isSubmitted = false;
        }

        function onReset(){
            regCtrlself.user = Object.assign({}, regCtrlself.user);
            regCtrlself.registrationform.$setPristine();
            regCtrlself.registrationform.$setUntouched();
        }

        function onSubmit(){
            console.log(RegServiceAPI);
            RegServiceAPI.register(regCtrlself.user)
                .then((result) => {
                    regCtrlself.user = result;
                    console.log($log);
                    $log.info(result);
                    regCtrlself.isSubmitted = true;
                }).catch((error) => {
                    console.log(error);
                })
        }

        regCtrlself.initForm();
    }
    
})();