"use strict";
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");
var moment = require("moment");
moment().format();

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT || 4000;

app.use(express.static(__dirname + "/../client/"));

app.post("/api/register", (req, res) => {
    var registeredUser = req.body;
    console.log("Email > " + registeredUser.email);
    console.log("Password > " + registeredUser.password);
    console.log("Confirm Password > " + registeredUser.confirmpassword);
    console.log("Full Name > " + registeredUser.fullName);
    console.log("Gender > " + registeredUser.gender);

    console.log("Date of birth > " + registeredUser.dob);
    var newDob = moment(registeredUser.dob);
    console.log(newDob.toLocaleString());

    console.log("Address > " + registeredUser.address);
    console.log("Nationality > " + registeredUser.selectedNationality);
    console.log("Contact Number > " + registeredUser.contactNumber);
    res.status(200).json(registeredUser);
});

app.use(function (req, res) {
    res.send("<h1>Page not found!</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});


//make app public
module.exports = app